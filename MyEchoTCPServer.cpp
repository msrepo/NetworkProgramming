#include "PracticalSocket.h"
#include "miscellaneous.h"
#include <iostream>
int main(){

    unsigned short port = 12345;
    TCPServerSocket myServerSocket(port);

    while(1){
        TCPSocket * newConnection = myServerSocket.accept();
        std::cout<<"Received connection request from "<<newConnection->getForeignAddress()<<":"<<newConnection->getForeignPort()<<'\n';
        const int RECVBUFFSIZE = 32;
        char recvbuffer[RECVBUFFSIZE];

        
        int bytesreceived;
        bytesreceived = newConnection->recv(recvbuffer,RECVBUFFSIZE);
        
        newConnection->send(recvbuffer,bytesreceived);
    }
    return 0;
}