#include <iostream>
#include <cstdint>
struct message{
    uint8_t oneByte;    //padding
    uint16_t twoByte;
    uint32_t fourByte;
    uint64_t eightByte;

};
int main(){

    std::cout<<sizeof(message)<<" bytes"<<std::endl;
    return 0;
}