#include <iostream>

struct personal_details{
    char first_name[10];
    int age;
    struct personal_details * parents[2];
};
typedef struct personal_details pd;
//pass pointers
void print_details(struct personal_details * person);
int main(){

    struct personal_details p1 = {"ram", 20};

    std::cout<<"Name:"<<p1.first_name<<std::endl;
    std::cout<<"Age:"<<p1.age<<std::endl;

    struct personal_details sita = {"sita",22};
    print_details(&sita);

    pd hari = {"hari",20};
    print_details(&hari);
    return 0;
}

void print_details(struct personal_details * person){

    std::cout<<"Name:"<<(*person).first_name<<std::endl;
    std::cout<<"Age:"<<person->age<<std::endl;
}