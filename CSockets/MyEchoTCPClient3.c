
// connection oriented client
//
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
void diewithmessage(char * message){
	printf("%s\n",message);
	exit(1);
}
int main(int argc, char **argv){
	int returncode;
	if(argc < 4 )
	{
		printf("Usage: %s <ServerIP> <serverPort> <echoword>",argv[0]);
		exit(1);
	}
	char * serverIP = argv[1];
	unsigned short serverPort = atoi(argv[2]);
	char * echoword = argv[3];
	// 1. Find the IP Address and protocol number of the server
	
	// 2. allocate a socket
	int mysocket = socket(PF_INET,SOCK_STREAM,0);
	if(mysocket < 0){
		diewithmessage("socket() failed.");
	}
	// 3. specify that the connection needs an arbitrary unused port on local machine and allow tcp to choose one
	
	struct sockaddr_in serveraddress;
	memset(&serveraddress,0,sizeof(serveraddress));
	serveraddress.sin_family=AF_INET;
	serveraddress.sin_port = htons(serverPort);
	returncode = inet_pton(AF_INET,serverIP,&serveraddress.sin_addr.s_addr);
	if(returncode == 0){
		diewithmessage("inet_pton() failed. could not convert address string.");
	}
	//	serveraddress.sin_addr.s_addr = serverIP;

	// 4. connect the socket to the server
	returncode = connect(mysocket,(struct sockaddr * )&serveraddress,sizeof(serveraddress));
	if(returncode < 0){
		diewithmessage("connect() failed.");
	}

	
	// 5. send / recieve request 
	write(mysocket,echoword,strlen(echoword));
	
	int echolength = strlen(echoword);
	int totalbytesreceived = 0,bytesreceived = 0;
	int MAXBUFFERSIZE = 30;
	char buffer[MAXBUFFERSIZE+1];
	char * readbuffer = buffer;
	int bufferlength = MAXBUFFERSIZE;
	while(totalbytesreceived < echolength){
		bytesreceived = read(mysocket, readbuffer, bufferlength);
		readbuffer +=bytesreceived;
		bufferlength -=bytesreceived;
		totalbytesreceived +=bytesreceived;

	}
	
	buffer[totalbytesreceived] = '\0';
	fputs(buffer,stdout);

	// 6. close the connection
	close(mysocket);
	

}

