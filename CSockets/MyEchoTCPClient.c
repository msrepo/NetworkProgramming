#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

int main(int argc, char * argv[]){

    char * serverIP;
    uint16_t serverPort;
    char * echoString;

    if(argc < 4){
        printf("Usage: MyEchoTCPClient serverIP serverPort Message \n");
        printf("Example: MyEchoTCPClient 127.0.0.1 12345 Hello\n");   
        exit(0);
    }else{
        serverIP = argv[1];
        serverPort = atoi(argv[2]);
        echoString = argv[3];
    }
    //create a reliable stream socket using TCP
    int mysocket = socket(AF_INET, SOCK_STREAM,IPPROTO_TCP);

    struct sockaddr_in serverAddress;
    serverAddress.sin_family = AF_INET;

    int returnValue = inet_pton(AF_INET,serverIP,&serverAddress.sin_addr.s_addr);

    connect(mysocket, &serverAddress,sizeof(serverAddress));


    int numBytes = send(mysocket, echoString, strlen(echoString), 0);
    assert(numBytes == strlen(echoString));

    const int RECVBUFFSIZE = 32;
    char buffer[RECVBUFFSIZE+1];
    numBytes = recv(mysocket, buffer, RECVBUFFSIZE,0);    
    if(numBytes == 0){
        printf("End of Transmission from the Sender.\n");
    }
    close(mysocket);
    return 0;
}
