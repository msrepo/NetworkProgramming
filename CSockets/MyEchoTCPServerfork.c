#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
void handleTCPclient(int clientsocket_);
void diewithmessage(char * message){

	printf("%s\n",message);
	exit(1);
}
int main(int argc, char **argv){

	if(argc < 2){
		printf("Usage: %s <serverPort>",argv[0]);
		exit(1);
	}
	int returncode;

	unsigned short serverPort = atoi(argv[1]);
//1. allocate a socket and bind to the well-known address for the service being offered
	int mysocket = socket(AF_INET,SOCK_STREAM,0);
	if(mysocket < 0){
		diewithmessage("socket() failed.");
	}
	struct sockaddr_in serveraddress;
	memset(&serveraddress,0, sizeof(serveraddress));
	serveraddress.sin_family = AF_INET;
	serveraddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddress.sin_port = htons(serverPort);
//2. place the socket in passive mode, making it ready for use by a server
	returncode = 	bind(mysocket,(struct sockaddr*)&serveraddress,sizeof(serveraddress));	
	if(returncode < 0){
		diewithmessage("bind() failed.");
	}
	returncode = 	listen(mysocket,0);
	if(returncode < 0){
		diewithmessage("listen() failed.");
	}
	while(1){
//3. accept the next connection request from the socket, and obtain a new socket for the connection
	struct sockaddr_in clientaddress;
	socklen_t clientaddresslen;
	int clientsocket = accept(mysocket,(struct sockaddr *)&clientaddress,&clientaddresslen);
	
	if(clientsocket < 0){
		diewithmessage("accept() failed.");
	}
	char clientIP[INET_ADDRSTRLEN];
	if(inet_ntop(AF_INET, &clientaddress.sin_addr.s_addr,clientIP,sizeof(clientIP)) != NULL)
		printf("Recieved request from client %s : %d\n",clientIP,ntohs(clientaddress.sin_port));
	else
		printf("Unable to get client address\n");
	if(fork() == 0){ /* child process */
		close(mysocket);
		handleTCPclient(clientsocket);
		exit(0);

	}else{
		close(clientsocket);
//5. when finished with a particular client, close the connection and return to step3 to accept a new connection
	
	}
	//4. repeatedly read a request from the client, formulate a response, and send a reply back to the client according to the application protocol
}

}


void handleTCPclient(int clientsocket_){
	int numbytesreceived;
	int MAXBUFFERSIZE = 32;
	int buffer[MAXBUFFERSIZE+1];
	
	do{
		numbytesreceived = read(clientsocket_,buffer,MAXBUFFERSIZE);

		printf("%d bytes recieved from client.\n",numbytesreceived);

		write(clientsocket_,buffer,numbytesreceived);

	}while(numbytesreceived > 0);

	if(numbytesreceived == 0) close(clientsocket_);
}
