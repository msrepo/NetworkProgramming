#include <sys/socket.h>
//#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <unistd.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>
int main(){

	char message[] ="Hello";
	const int MAXBUFFSIZE = 20;
	char buffer[MAXBUFFSIZE+1];
	int bytesrecvd = 0;
	//1. socket();
	int mysocket = socket(PF_INET,SOCK_STREAM,IPPROTO_TCP);
	//socket(PF_INET, SOCK_DGRAM,IPPROTO_UDP);

	struct sockaddr_in serveraddress;
	bzero(&serveraddress,sizeof(serveraddress));
	serveraddress.sin_family = AF_INET;
	serveraddress.sin_port =htons( 12345);
	//serveraddress.sin_addr.s_addr = "127.0.0.1";
	inet_pton(AF_INET,"127.0.0.1",(struct sockaddr *)&serveraddress.sin_addr.s_addr);

	//2. connect();
	//connect(mysocket, &serveraddress,sizeof(serveraddress);
	int returncode = connect(mysocket,(struct sockaddr *)&serveraddress,INET_ADDRSTRLEN);
	if(returncode == -1)printf("connect() failed");
	//3. read() / write();
	send(mysocket,message,sizeof(message),0);
	bytesrecvd = recv(mysocket,buffer,MAXBUFFSIZE,0);
	buffer[bytesrecvd] = 0;
	printf("received %d from server.\n",bytesrecvd);
	printf("Received %s from server.\n",buffer);

	//4.close();
	close(mysocket);



	return 0;
}
