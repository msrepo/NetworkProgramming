#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "network_headers.h"


int main(){

	int mysocket = socket(AF_INET,SOCK_DGRAM,0);

	struct sockaddr_in myaddress;
	myaddress.sin_family = AF_INET;
	myaddress.sin_port = 14640;
	myaddress.sin_addr.s_addr = htonl(INADDR_ANY);
	//	myaddress.sin_addr.s_addr = htonl(INADDR_ANY);
//	inet_pton(AF_INET,"192.168.137.255",&myaddress.sin_addr.s_addr);

	int returncode = bind(mysocket,(struct sockaddr *)&myaddress,sizeof(myaddress));
	
//	listen(mysocket,0);
	if(returncode < 0) fprintf(stderr,"bind() failed %s",strerror(errno)),exit(1);
		//perror("bind() failed"), exit(1);

	int MAXBUFFSIZE = 32;
	char recvbuffer[MAXBUFFSIZE+1];
	int bytesrecvd = 0;
	while(1){

		bytesrecvd = recvfrom(mysocket,recvbuffer,MAXBUFFSIZE,0,NULL,NULL);
		recvbuffer[bytesrecvd] = 0;
		printf("Received %s\n",recvbuffer);
	}
	return 0;
}
