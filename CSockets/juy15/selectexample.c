#include "network_headers.h"
#include <sys/time.h>

int main(){

	struct timeval duration;
	fd_set readfiledescriptors;

	duration.tv_sec = 2;
	duration.tv_usec =500000;

	FD_ZERO(&readfiledescriptors);
	FD_SET(0,&readfiledescriptors);

	//dont care about writefds and exceptionfds
	while(1){
		printf(".");fflush(stdout);
		sleep(3); //3 seconds
	select(1,&readfiledescriptors,NULL,NULL,&duration);
	
	if(FD_ISSET(0,&readfiledescriptors))
		printf("A key was pressed\n");
	else
		printf("Nothing received yet.\n");


	}
	
	return 0;
}
