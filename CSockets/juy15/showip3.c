#include "network_headers.h"
#include <stdio.h>
#include <netdb.h>
#include <sys/socket.h>
#include <arpa/inet.h>

int main(int argc, char ** argv){

	struct addrinfo hints, *result;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	
	getaddrinfo(argv[1],NULL,&hints,&result);
	
	while(result != NULL){
	struct sockaddr_in * ipaddress = (struct sockadrr_in *)(*result).ai_addr;
	char ipaddressstring[INET_ADDRSTRLEN];
	
	inet_ntop(AF_INET,&(ipaddress->sin_addr),ipaddressstring,sizeof ipaddressstring);
	printf("IP address for %s :%s\n",argv[1],ipaddressstring);
	
	result = result->ai_next;
	}

	return 0;
}
