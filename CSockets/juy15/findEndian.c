#include <stdio.h>


int main(){

	union endiancheck{
		char bytes[2];
		unsigned short sdata;

	};

	union endiancheck data;
	data.sdata =0X0c0d;

	printf("%hhx %hhx\n",data.bytes[1],data.bytes[0]);
	if(data.bytes[1] == 0x0c) printf("Little Endian/host byte order");
	else printf("Big Endian/network byte order");
	return 0;
}
