#include "network_headers.h"
#include <stdio.h>
#include <netdb.h>
#include <sys/socket.h>
#include <arpa/inet.h>

int main(int argc, char ** argv){

	struct addrinfo hints, *result;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET6;
	hints.ai_socktype = SOCK_STREAM;
	
	getaddrinfo(argv[1],NULL,&hints,&result);
	
	while(result != NULL){
	struct sockaddr_in6 * ipaddress = (struct sockaddr_in6 *)(*result).ai_addr;
	char ipaddressstring[INET6_ADDRSTRLEN];
	
	inet_ntop(AF_INET6,&(ipaddress->sin6_addr),ipaddressstring,sizeof ipaddressstring);
	printf("IP address for %s :%s\n",argv[1],ipaddressstring);
	
	result = result->ai_next;
	}

	return 0;
}
