#include "network_headers.h"

int main(int argc, char **argv){

	struct addrinfo hints, *result, *temp;

	memset(&hints,0, sizeof hints);	//fillup details
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	
	getaddrinfo(argv[1],NULL,&hints,&result);

	printf("IP addresses for %s:\n\n",argv[1]);

	//read info from linked list
	for(temp = result; temp != NULL; temp = temp ->ai_next){
		
		char ipaddress[INET_ADDRSTRLEN];
		struct sockaddr_in *ipv4structure;
		ipv4structure = (struct sockaddr_in *)temp->ai_addr;
		inet_ntop(temp->ai_family,&(ipv4structure->sin_addr),
				ipaddress,sizeof ipaddress);
		printf("%s\n",ipaddress);
	}

	freeaddrinfo(result);


	return 0;
}
