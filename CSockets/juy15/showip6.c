#include <stdio.h>
#include <sys/socket.h>

#include <netdb.h>

int main(int argc, char **argv){

	char *name = argv[1];

	struct addrinfo hints;
	struct addrinfo *result;
	memset(&hints,0 , sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	getaddrinfo(name,NULL,&hints,&result);

	char ipaddres_string[INET_ADDRSTRLEN];
	while(result != NULL){
		
	struct sockaddr_in * ipaddrstruct = (*result).ai_addr;
	inet_ntop(AF_INET,&(ipaddrstruct->sin_addr),ipaddres_string,sizeof ipaddres_string);
	printf("IP address of %s is %s\n",name,ipaddres_string);
//	printf("%ul",(ipaddrstruct->sin_addr).s_addr);
	result  = result->ai_next;
	}
	return 0;
}



