#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>  /* sockaddr_in structure */
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>

void diewitherrormessage(char * message){
	printf("%s\n",message);exit(1);
}
int main(int argc, char **argv){

	int mysocket = socket(AF_INET,SOCK_DGRAM,0);
	
	struct sockaddr_in destinationaddress;
	destinationaddress.sin_family = AF_INET;
	destinationaddress.sin_addr.s_addr = inet_addr("255.255.255.255");
	//inet_pton(AF_INET,"255.255.255.255",&destinationaddress.sin_addr);
	destinationaddress.sin_port = htons(12345); // network byte order for 12345
	
	int broadcastpermission = 1;
	int returncode = setsockopt(mysocket,SOL_SOCKET,SO_BROADCAST,&broadcastpermission,sizeof(broadcastpermission));
	if(returncode < 0) diewitherrormessage("setsockopt() failed");

	while(1){

		sendto(mysocket,argv[1],sizeof argv[1],0,(struct sockaddr *)&destinationaddress,sizeof(destinationaddress));
		sleep(3);
	}

	//NEVER REACHED
	return 0;
}
