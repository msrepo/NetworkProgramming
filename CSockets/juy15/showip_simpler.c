#include "network_headers.h"
#include <stdio.h>

int main(int argc, char **argv){

	struct addrinfo hints, *result;

	memset(&hints,0,sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	
	getaddrinfo(argv[1], NULL,&hints,&result);

	printf("IP address for %s:",argv[1]);
	struct sockaddr_in * ipaddr = result->ai_addr;
	char ipaddress[INET_ADDRSTRLEN];

	inet_ntop(ipaddr->sin_family,&(ipaddr->sin_addr).s_addr,ipaddress,sizeof ipaddress);
	printf("%lu\n",(ipaddr->sin_addr).s_addr);
	printf("%s\n",ipaddress);
	return 0;
}

