
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <arpa/inet.h>

void diewitherrormessage(char * message){
	printf(message);
	exit(1);
}

int main(int argc, char **argv){

	if(argc <3){
		printf("Usage: port message");
		exit(1);
	}

	int returncode;
	//read port
	int port = htons(atoi(argv[1]));

	struct sockaddr_in destination_address;
	memset(&destination_address,0,sizeof(destination_address));
	destination_address.sin_family = AF_INET;
	destination_address.sin_port  = port;
	destination_address.sin_addr.s_addr = INADDR_BROADCAST;
	

	int messagelength = strlen(argv[2]);

	//create socket
	int mysocket = 	socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
	if(mysocket < 0) diewitherrormessage("socket() FAILED");
	//get permission to broadcast
	int broadcast_permission = 1;
	returncode =	setsockopt(mysocket,SOL_SOCKET,SO_BROADCAST,&broadcast_permission,sizeof(broadcast_permission));
	if(returncode < 0) diewitherrormessage("setsockopt() failed.");

	int numbytes;
	while(1){
		numbytes = sendto(mysocket,argv[2],messagelength,0,&destination_address,sizeof(destination_address));
		if(numbytes < 0) diewitherrormessage("sendto() failed.");
		sleep(3); //send every 3 seconds


	}
	return 0;
}
