#include <sys/types.h>
#include <sys/socket.h>	
#include <arpa/inet.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
int main(int argc, char * argv[]){

	char * serverIP = argv[1];
	uint16_t serverPort = atoi(argv[2]);
	char * echoString = argv[3];
	printf("%s %u %s\n",serverIP,serverPort,echoString);
	int mysocket = 	socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	
	
	if(mysocket < 0) 
		printf("socket() call failed.\n");
	
	struct sockaddr_in serverAddress;
	serverAddress.sin_family = AF_INET;
	inet_pton(AF_INET,serverIP,&serverAddress.sin_addr.s_addr);
	
	connect(mysocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress));
		
	
	int numBytesSent = send(mysocket, echoString, strlen(echoString), 0);
	if(numBytesSent < 0)
		printf("send() failed.\n");
	else if(numBytesSent != strlen(echoString))
		printf("Connection closed prematurely.\n");

	const int RECVBUFFSIZE = 32;
	char recvbuffer[RECVBUFFSIZE+1];

	int numBytesrecvd = recv(mysocket,recvbuffer,RECVBUFFSIZE,0);
	printf("%d received from server\n",numBytesrecvd);

	recvbuffer[numBytesrecvd] = '\0';
	puts(recvbuffer);	


	close(mysocket);
	return 0;
}
