#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <stdio.h>

void diewitherrormessage(char *message){
	printf("%s\n",message);
	exit(1);
}
int main(){
	int returncode;
//1.socket
	int mysocket = socket(AF_INET ,SOCK_STREAM,0 );
	if(mysocket < 0){
		diewitherrormessage("socket() failed");
	}
	struct sockaddr_in serveraddress;
	unsigned short serverPort = 12345;
	char serverIP[] ="127.0.0.1"; 
	serveraddress.sin_family = AF_INET;
	serveraddress.sin_port = htons(serverPort);
	returncode = inet_pton(AF_INET,serverIP,&serveraddress.sin_addr.s_addr);
	if(returncode != 1){
		diewitherrormessage("inet_pton() failed");
	}	
//2. connect
	returncode = connect( mysocket, (struct sockaddr *)&serveraddress,sizeof(serveraddress) );
	if(connect < 0 ){
		diewitherrormessage("connect() failed");
	}
	//3. write
	char message[] = "hello";
	send(mysocket,message, sizeof(message),0);

	const int MAXBUFFERSIZE = 30;
	char buffer[MAXBUFFERSIZE + 1];
	//4. read
	recv(mysocket,buffer,sizeof(buffer),0);
	
	printf("Received %s from server.\n",buffer);
	//5. close
	close(mysocket);
}

