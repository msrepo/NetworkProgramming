#include <sys/socket.h>

int main(){

	//create a TCP Socket
	int mysocket = socket(PF_INET,SOCK_STREAM,IPPROTO_TCP);

	unsigned short serverPort = 12345;
	
	//fill in the endpoint address
	struct sockaddr_in serverAddress;
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr = INADDR_ANY;
	serverAddress.sin_port = serverPort;

	//bind the address to mysocket
	bind(mysocket,&serverAddress,sizeof(serverAddress));


	//mark the socket as accepting connection
	listen(mysocket);

	while(1){
		//accept new connection
		int newconnection = accept(mysocket, NULL,NULL);

		const int MAXBUFFSIZE = 32;
		int numbytesrecvd;
		char buffer[MAXBUFFSIZE+1];
		numbytesrecvd = recv(newconnection, buffer, MAXBUFFSIZE);
		


	}

	return 0;
}
