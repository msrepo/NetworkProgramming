#include <stdio.h>

void useIdleTime(){

	printf(".");
	fflush(stdout);
	sleep(2); //sleep for 2 seconds
}

int main(){

	for(;;)
		useIdleTime();


	return 0;
}
