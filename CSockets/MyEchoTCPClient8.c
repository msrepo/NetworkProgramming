
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
void diewitherrormessage(char *message){
	printf("%s\n",message);
	exit(1);
}
int main(){

	int returncode,bytesrecvd;
	
	//1. allocate socket()
	int mysocket = socket( AF_INET, SOCK_STREAM, 0 );
	if(mysocket < 0){
		diewitherrormessage("socket() failed");
	}
	//	2. define where i want to connect
	struct sockaddr_in serveraddress;
	memset(&serveraddress,0,sizeof(serveraddress));
	serveraddress.sin_port =htons( 12345);
	serveraddress.sin_family = AF_INET;
	//	serveraddress.sin_addr.s_addr = "127.0.0.1";
	returncode = inet_pton(AF_INET,"127.0.0.1",(struct sockaddr *)&serveraddress.sin_addr.s_addr);
	if(returncode == -1){
		printf("Could not convert to binary format. inet_pton() failed");
		exit(1);
	}
	//	3. onnect() actively create a connection
	returncode = connect(mysocket,(struct sockaddr *) &serveraddress , sizeof(serveraddress));
	if(returncode == -1){
		diewitherrormessage("connect() failed");
	}
	do{
	//	4. send() recv()  read() write()
		printf("Enter a echo message(less than 80 chars):");
		
		char message[81];
		scanf("%s",message);
		int msgsize = strlen(message);
		printf("read %d characters from terminal.\n",msgsize);
		send(mysocket, message,msgsize,0);
		const int MAXBUFFERSIZE = 80;
		char buffer[MAXBUFFERSIZE+1];
		int totalbytesrecvd = 0;
		while(totalbytesrecvd < msgsize){
		 bytesrecvd = recv(mysocket,buffer+totalbytesrecvd,MAXBUFFERSIZE,0);
		 totalbytesrecvd +=bytesrecvd;
		}
		buffer[totalbytesrecvd] = '\0';
		printf("Received %s(%d bytes) from server\n",buffer, bytesrecvd);

	}while(bytesrecvd != 0);
//	5. close()
	close(mysocket);
	return 0;
}
