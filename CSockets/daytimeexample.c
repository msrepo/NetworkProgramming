#include <time.h>
#include <stdio.h>

int main(){
	time_t current_time = 0;

	time(&current_time);
	printf("%ld\n",current_time);
	char * current_time_in_string_fmt = ctime(&current_time);
	printf("%s\n",current_time_in_string_fmt);
	return 0;
}
