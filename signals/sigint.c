#include <signal.h>
#include <stdio.h>
#ifdef WIN32
#include <windows.h>          // For ::Sleep()
void sleep(unsigned int seconds) {::Sleep(seconds * 1000);}
#else
#include <unistd.h>           // For sleep()
#endif
void myFunction(){
    printf("Hello World!\n");
    sleep(3);   //for 3 seconds
}
int main(){

    signal(SIGINT, myFunction);

    for(int i = 0; i < 10; i++){
        printf("%d\n",i);
        sleep(3);
    }

    return 0;
}