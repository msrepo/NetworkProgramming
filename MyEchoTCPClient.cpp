#include "PracticalSocket.h"
#include <iostream>
#include <string>
// #include <cstdlib>
int main(){

    const string serverIP = "127.0.0.1";
    unsigned short serverPort = 12345;
    TCPSocket mySocket(serverIP,serverPort);

    string message = "hello";
    // string message = "नमस्ते";
    const int messagelength = message.length();
    mySocket.send(message.c_str(),messagelength);
    
    const int RECVBUFFSIZE = 32;
    char recvbuffer[RECVBUFFSIZE];
    mySocket.recv(recvbuffer,RECVBUFFSIZE);


    std::cout<<"Received from Server:"<<recvbuffer<<'\n';
    return 0;
}