#include<stdio.h>
#include<stdbool.h>
#define EMPTY 0
#define P1 'O'
#define P2 'X'

int board[3][3]; 	//is global; set to zero automatically
void printboard();
int isgameover();
int isrowsmatch();
int iscolumnsmatch();
int isdiagonalsmatch();
int isrowempty();
int islegalmove(int row, int col);

int main(){
	int row, column;	
	printboard();
	while(1){

		do{
		printf("Player 1 moves:");
		scanf("%d,%d",&row,&column);
		}while(!islegalmove(row,column));

		board[row-1][column-1] = P1;
		printboard();
		if(isgameover()) break;
		do{
		printf("Player 2 moves:");
		scanf("%d,%d",&row,&column);
		}while(!islegalmove(row,column));

		board[row-1][column-1] = P2;
		printboard();
		if(isgameover()) break;
	}
	printf("GAME OVER\n");
	return 0;
}

int islegalmove(int row, int column){
	if(row > 3 || row < 1) {
		printf("Illegal move.Enter values in row[1-3],column[1,3] format. Example: 3,3\n");
		return false;
	}
	if(column  > 3 || column  < 1){
		printf("Illegal move.Enter values in row[1-3],column[1-3] format. Example: 3,3\n");
		return false;
	}

	if(board[row-1][column-1] != 0) //already exists
	{
		printf("this position has already been used\n");
		return false;
	}
	return true;
}
void printboard(){
	printf("\n");
	printf("-------------\n");
	int row = 0;
	for (; row < 3; row++)
	{	
		printf("|%3c|%3c|%3c|\n",board[row][0],board[row][1],board[row][2]);
		printf("-------------\n");

	}
}

int isgameover(){
	return isrowsmatch()==true || iscolumnsmatch() == true || isdiagonalsmatch() == true;
}
int isrowempty(int row){
	int column = 0;
	for( ; column < 3; column++ )
	if(board[row][column] != 0)
		return false;
	return true;
}
int isrowsmatch(){
	int row = 0;
	for( ; row < 3; row++ )
	{
		if(isrowempty(row)) continue;
		if(board[row][0] == board[row][1] && board[row][0] == board[row][2])
			return true;
	}
	return false;
}

int iscolumnempty(int column){
	int row = 0;
	for( ; row < 3; row++ )
	if(board[row][column] != 0)
		return false;
	return true;
}
int iscolumnsmatch(){
	
	int column = 0;
	for( ; column < 3; column++ )
	{
		if(iscolumnempty(column)) continue;
		if(board[0][column] == board[1][column] && board[0][column] == board[2][column])
			return true;
	}
	return false;
}

int isdiagonalsmatch(){
	//is primary diagonal empty
	int row = 0, column = 0;
	int isprimarydiagonalempty = true;
	for ( ; row < 3; row++ )
	for ( ; column < 3; column++ )
	{
		if(row == column && board[row][column] != 0)
		{
			isprimarydiagonalempty = false;
			break;
		}			
	}
	
	if(!isprimarydiagonalempty && (board[0][0] == board[1][1] && board[0][0] == board[2][2]))
		return true;
	
	row = 0; column = 0;

	int issecondarydiagonalempty = true;
	for ( ; row < 3; row++ )
	for ( ; column < 3; column++ )
	{
		if(row + column  + 1 == 3  && board[row][column] != 0)
		{
			issecondarydiagonalempty = false;
			break;
		}			
	}
	if(!issecondarydiagonalempty && (board[0][2] == board[1][1] && board[2][0] == board[1][1]))
		return true;
	return false;
}
