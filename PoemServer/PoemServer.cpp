#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iomanip>
#include "../PracticalSocket.h"
using namespace std;
    typedef  vector<string> poem;
    typedef  vector<poem> poems;
void readfile(vector<poem> &);
int main(){

    vector<poem> poems;
    readfile(poems);
    cout<<"Successfully read "<<poems.size()<<" poem(s).";

    TCPServerSocket mySocket("127.0.0.1",12345);
    while(1){
        TCPSocket* newConnection = mySocket.accept();
        cout<<"No: of lines:"<<poems[0].size()<<endl;
        int messagesize = 0;

        for(int i = 0; i < poems[0].size(); i++){
            
            cout<<poems[0][i].c_str()<<endl;
            messagesize +=poems[0][i].size();
            // newConnection->send(poems[0][i].c_str(),poems[0][i].size());
        }
        newConnection->send(&messagesize,sizeof(messagesize));

        for(int i = 0; i < poems[0].size(); i++){
            std::cout<<"Sending "<<poems[0][i].size()<<" bytes"<<endl;
            newConnection->send(poems[0][i].c_str(),poems[0][i].size());
        }
    }
    return 0;
}

void readfile(vector<poem> &poems){
        //open file stream
    ifstream ifile("Poems.txt");
    // cout<<ifile.rdbuf();
    int numPoems,numlines;
    ifile>>numPoems;
    // cout<<numPoems<<endl;

    const int MAX = 1000;   //20 character * 50 lines
    char buffer[MAX];

    for(int i = 0; i < numPoems; i++){
        poem p;
        ifile>>numlines;
        // cout<<numlines;
        int bytesreceived = 0, totalbytesreceived = 0;
        for(int j=0; j < numlines; j++){
             ifile.getline(buffer,MAX);
            p.push_back(buffer);
            // cout<<buffer<<endl;
        }
        buffer[totalbytesreceived] = '\0';
        poems.push_back(p);
    }



}