#include "../PracticalSocket.h"
#include <iostream>
#include <string>
int main(){

    const string serverIP = "127.0.0.1";
    unsigned short serverPort = 12345;
    TCPSocket mySocket(serverIP,serverPort);

    int messagesize;
    mySocket.recv(&messagesize,sizeof(messagesize));
    std::cout<<"Going to receive "<<messagesize<<" bytes of data."<<endl;
    const int RECVBUFFSIZE = 80;
    char recvbuffer[RECVBUFFSIZE+1];
    int totalbytesreceived = 0, bytesreceived = 0;
    while(totalbytesreceived < messagesize){
        bytesreceived = mySocket.recv(recvbuffer,RECVBUFFSIZE);
        // std::cout<<"Received "<< bytesreceived<<" bytes"<<endl;
        recvbuffer[bytesreceived] = '\0';
        totalbytesreceived +=bytesreceived;
        std::cout<<recvbuffer<<endl;
    }
        std::cout<<endl;

    return 0;
}