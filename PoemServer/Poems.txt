1
20
Rain in Winter - Philip Levine
-------------------------------
Outside the window drops caught
on the branches of the quince, the sky
distant and quiet, a few patches of light
breaking through. The day is fresh, barely
begun yet feeling used. Soon the phone
will ring for someone, and no one
will pick it up, and the ringing will go on
until the icebox answers with a groan.

The lost dog who sleeps on a bed of rags
behind the garage won't appear
to beg for anything. Nothing will explain
where the birds have gone, why a wind rages
through the ash trees, why the world
goes on accepting more and more rain.

    from The Threepenny Review
