#include "PracticalSocket.h"
#include <iostream>
int main(){


	UDPSocket mySocket(12345);

	const int RECVBUFFERSIZE = 32;
	char recvBuffer[RECVBUFFERSIZE+1];
	string senderIP;
	unsigned short senderPort;
	//receive
	int bytesreceived = mySocket.recvFrom(recvBuffer,RECVBUFFERSIZE,senderIP,senderPort );
	

	std::cout<<"received from "<<senderIP<<":"<<senderPort<<" :"<<recvBuffer<<'\n';


	//send
	mySocket.sendTo(recvBuffer,bytesreceived,senderIP,senderPort);
	return 0;
}