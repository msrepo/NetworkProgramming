#include "PracticalSocket.h"
#include<iostream>
int main(){

	UDPSocket myudpsocket;

	string message = "hello";	//char message[]
	int messagelength = message.length();
	unsigned short senderport = 12345;
	// send
	//"hello"
	myudpsocket.sendTo( message.c_str(),messagelength, "127.0.0.1", senderport);


	string senderIP;
	unsigned short port;

	const int RECVBUFFSIZE = 32;
	char recvmsg[RECVBUFFSIZE];
	int recvmsgsize;
	//recieve
	recvmsgsize = myudpsocket.recvFrom( recvmsg, RECVBUFFSIZE  ,senderIP  ,port  );
	std::cout <<"Received "<<recvmsgsize <<" bytes from server:";
	std::cout<<recvmsg;
	return 0;
}