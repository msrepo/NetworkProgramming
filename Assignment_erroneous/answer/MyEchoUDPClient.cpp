/* UDP Echo Client */
#include "PracticalSocket.h"
#include <iostream>
int main(){
	UDPSocket myudpsocket;


	string message = "Hello";
	int messagelength = message.length();
	string serverIP = "127.0.0.1";
	unsigned short serverPort = 12345;
	//start sending
	myudpsocket.sendTo(message.c_str(),messagelength,serverIP,serverPort);

	
	const int RECVBUFFSIZE = 32;
	char recvmessage[RECVBUFFSIZE + 1];
	string senderIP;
	unsigned short senderPort;
	int bytesreceived;
	//and receiving right away
	bytesreceived = myudpsocket.recvFrom(recvmessage,RECVBUFFSIZE,senderIP,senderPort);
	recvmessage[bytesreceived] = '\0';
	std::cout<<"Received from Server:"<<recvmessage;


	return 0;
}