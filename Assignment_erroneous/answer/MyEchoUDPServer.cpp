/* UDP Echo Server */

#include "PracticalSocket.h"
#include <iostream>
int main(){

	unsigned short listenport = 12345;
	UDPSocket myudpsocket(listenport);

	const int RCVBUFFSIZE = 32;
	char recvbuffer[RCVBUFFSIZE+1];
	string senderIP;
	unsigned short senderPort;
	int bytesreceived;
	//start receiving
	bytesreceived = myudpsocket.recvFrom(recvbuffer, RCVBUFFSIZE,senderIP ,senderPort);

	std::cout<<"Received from "<<senderIP <<":"<<senderPort<<" "<<recvbuffer;
	//and sending right away
	myudpsocket.sendTo(recvbuffer,bytesreceived,senderIP,senderPort);


}