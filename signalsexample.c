#include <signal.h>
#include <stdio.h>
#ifdef WIN32
#include <windows.h>          // For ::Sleep()
void sleep(unsigned int seconds) {::Sleep(seconds * 1000);}
#else
#include <unistd.h>           // For sleep()
#endif
void myfunction(){
    printf("received sigint....\n");
}
int main(){
    int i;
    signal(SIGINT, myfunction);
    for(;;){
        printf("Hello\n");
    }
    return 0;
}